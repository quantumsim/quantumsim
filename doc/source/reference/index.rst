High-level interface
====================

The new `Circuit` class will go here. For temporary usage there is a module
:mod:`quantumsim.models`, but there is a plan to wipe it at Quantumsim-1.1
release.


Low-level interface
===================

.. toctree::
   :maxdepth: 1

   quantumsim.bases
   quantumsim.states
   quantumsim.circuits
   quantumsim.models

Internal modules
================

.. toctree::
   :maxdepth: 1

   quantumsim.algebra
