:mod:`quantumsim.models` -- definition of the gate set and its error model
==========================================================================

.. module:: quantumsim.models

Basic interface
---------------

.. autosummary::
   :toctree: generated/

   Model
   Setup
   WaitingGate

Predefined models
-----------------

.. autosummary::
   :toctree: generated/

   library.PerfectQubitModel
   library.PerfectQutritModel
